(defpackage cldezero/tests/main
  (:use :cl
        :cldezero
        :rove))
(in-package :cldezero/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :cldezero)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
