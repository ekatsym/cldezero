(defsystem "cldezero"
  :version "0.1.0"
  :author "ekatsym"
  :license ""
  :depends-on ("alexandria"
               "serapeum"
               "trivia")
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "util"        :depends-on ("package"))
                 (:file "array"       :depends-on ("package" "util"))
                 (:file "core-simple" :depends-on ("package" "util" "array"))
                 (:file "dot"         :depends-on ("package" "util" "core-simple"))
                 )))
  :description ""
  :in-order-to ((test-op (test-op "cldezero/tests"))))

(defsystem "cldezero/tests"
  :author "ekatsym"
  :license ""
  :depends-on ("cldezero"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for cldezero"
  :perform (test-op (op c) (symbol-call :rove :run c)))
