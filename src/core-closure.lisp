(defpackage cldezero.core
  (:use :common-lisp)
  (:import-from :cldezero.closure
                #:reflective-let
                #:dispatch-lambda
                #:self
                #:csref
                #:with-closure)
  (:import-from :cldezero.array
                #:map-array)
  (:import-from :alexandria
                #:with-unique-names)
  (:export #:self
           #:csref
           #:with-closure

           #:var
           #:data
           #:name
           #:creator
           #:generation
           #:id

           #:define-fun-constructor
           #:inputs
           #:output
           #:dinputs
           #:doutput

           #:add
           #:mul
           #:neg
           #:sub
           #:div
           #:pow))
(in-package :cldezero.core)


;;; VAR
(defun var (data &optional (name nil))
  (check-type data (array number))
  (check-type name symbol)
  (reflective-let ((id (random 1000000000000))
                   (data data)
                   (name name)
                   (grad nil)
                   (creator nil)
                   (generation 0))
    (dispatch-lambda
      (:typecheck (type) (eq type 'var))
      (:get (param)
       (ecase param
         (data data)
         (name name)
         (grad grad)
         (creator creator)
         (generation generation)
         (id id)))
      (:set (param value)
       (ecase param
         (data (setq data value))
         (name (setq name value))
         (grad (setq grad value))
         (creator (setq creator value))
         (generation (setq generation value))))
      (:dimensions ()
       (array-dimensions data))
      (:rank ()
       (array-rank data))
      (:total-size ()
       (array-total-size data))
      (:element-type ()
       (array-element-type data))
      (:dimension (axis-number)
       (array-dimension data axis-number))
      (:print (&optional (stream *standard-output*))
       (if (null data)
           (format stream "#<VAR NIL>")
           (format stream "#<VAR ~S>" data)))
      (:set-creator (fun)
       (setq creator fun)
       (incf generation 1)
       creator)
      (:cleargrad ()
       (setq grad nil))
      (:backward (&optional (retain-grad nil))
       (unless grad
         (setq grad (make-array (array-dimensions data) :initial-element 1)))
       (labels ((add-fun (fun funs)
                  (do ((gen (csref fun 'generation))
                       (tail funs (rest tail))
                       (rhead '() (cons (first tail) rhead)))
                      ((or (endp tail) (> gen (csref (first tail) 'generation)))
                       (revappend rhead (cons fun tail)))
                      (when (eq fun (first tail))
                        (return funs))))
                (collect-funs (fun)
                  (do ((funs (list fun) (mapcan (lambda (f)
                                                  (remove nil
                                                          (mapcar (lambda (v)
                                                                    (csref v 'creator))
                                                                  (csref f 'inputs))))
                                                funs))
                       (acc '() (reduce (lambda (acc f) (add-fun f acc))
                                        funs
                                        :initial-value acc)))
                      ((endp funs) acc))))
         (dolist (fun (collect-funs creator))
           (declare (type function fun))
           (let* ((dl/dy (csref (csref fun 'output) 'grad))
                  (dy/dxs (multiple-value-list
                            (apply fun :derivative
                                   (mapcar (lambda (in) (csref in 'data))
                                           (csref fun 'inputs)))))
                  (dl/dxs (mapcar (lambda (dy/dx) (map-array #'* dl/dy dy/dx)) dy/dxs)))
             (mapc (lambda (in dl/dx)
                     (if (null (csref in 'grad))
                         (setf (csref in 'grad) dl/dx)
                         (setf (csref in 'grad) (map-array #'+ (csref in 'grad) dl/dx))))
                   (csref fun 'inputs)
                   dl/dxs)
             (unless retain-grad
               (setf (csref fun 'output) nil)))))))))

(defun varp (object)
  object
  (handler-case (funcall object :typecheck 'var)
    (error nil)))

(deftype var ()
  '(satisfies varp))

(defun ensure-var (object)
  (etypecase object
    (number         (var (vector object)))
    ((array number) (var object))
    (var            object)))

(defun ensure-vector (object)
  (etypecase object
    (number         (vector object))
    ((array number) object)))


;;; FUN
;; Definition
(defmacro define-fun-constructor (name lambda-list (&rest bindings) &body body &key function derivative)
  (declare (ignore body))
  (with-unique-names (g!inputs g!output g!generation g!function g!derivative
                      g!args g!input g!param g!value)
    `(defun ,name ,lambda-list
       (reflective-let ((,g!inputs nil)
                        (,g!output nil)
                        (,g!generation 0)
                        ,@bindings)
         (let ((,g!function ,function)
               (,g!derivative ,derivative))
           (dispatch-lambda
             (:function (&rest ,g!args)
              (apply ,g!function ,g!args))
             (:derivative (&rest ,g!args)
              (apply ,g!derivative ,g!args))
             (:call (&rest ,g!args)
              (setq ,g!inputs (mapcar #'ensure-var ,g!args)
                    ,g!output (var (apply ,g!function
                                          (mapcar (lambda (,g!input) (csref ,g!input 'data))
                                                  ,g!inputs)))
                    ,g!generation (reduce #'max ,g!inputs
                                          :key (lambda (,g!input) (csref ,g!input 'generation))))
              (funcall ,g!output :set-creator #'self)
              ,g!output)
             (:get (,g!param)
              (ecase ,g!param
                (inputs ,g!inputs)
                (output ,g!output)
                (generation ,g!generation)
                ,@(mapcar (lambda (binding)
                            `(,(first binding) ,(first binding))) bindings)
                (self #'self)))
             (:set (,g!param ,g!value)
              (ecase ,g!param
                (inputs (setq ,g!inputs ,g!value))
                (output (setq ,g!output ,g!value))
                (generation (setq ,g!generation ,g!value))
                ,@(mapcar (lambda (binding)
                            `(,(first binding) (setq ,(first binding) ,g!value)))
                          bindings)))))))))

;; ADD
(define-fun-constructor %add2 () ()
  :function   (lambda (x y)
                (map-array #'+ x y))
  :derivative (lambda (x y)
                (values (make-array (array-dimensions x) :initial-element 1)
                        (make-array (array-dimensions y) :initial-element 1))))

(defun add2 (x y)
  (funcall (%add2) :call x y))

(defun add (x &rest xs)
  (reduce #'add2 xs :initial-value x))

;; MUL
(define-fun-constructor %mul2 () ()
  :function   (lambda (x y)
                (map-array #'* x y))
  :derivative (lambda (x y)
                (values y x)))

(defun mul2 (x y)
  (funcall (%mul2) :call x y))

(defun mul (x &rest xs)
  (reduce #'mul2 xs :initial-value x))

;; NEG
(define-fun-constructor %neg () ()
  :function   (lambda (x)
                (map-array #'- x))
  :derivative (lambda (x)
                (make-array (array-dimensions x) :initial-element -1)))

(defun neg (x)
  (funcall (%neg) :call x))

;; SUB
(define-fun-constructor %sub2 () ()
  :function   (lambda (x y)
                (map-array #'- x y))
  :derivative (lambda (x y)
                (values (make-array (array-dimensions x) :initial-element 1)
                        (make-array (array-dimensions y) :initial-element -1))))

(defun sub2 (x y)
  (funcall (%sub2) :call x y))

(defun sub (x &rest xs)
  (reduce #'sub2 xs :initial-value x))

;; DIV
(define-fun-constructor %div2 () ()
  :function   (lambda (x y)
                (map-array #'/ x y))
  :derivative (lambda (x y)
                (values (map-array #'/ y)
                        (map-array (lambda (xi yi) (- (/ xi (* yi yi)))) x y))))

(defun div2 (x y)
  (funcall (%div2) :call x y))

(defun div (x &rest xs)
  (reduce #'div2 xs :initial-value x))

;; POW
(define-fun-constructor %pow (n) ((n n))
  :function   (lambda (x) (map-array (lambda (xi) (expt xi n)) x))
  :derivative (lambda (x) (map-array (lambda (xi) (* n (expt xi (1- n)))) x)))

(defun pow (x n)
  (funcall (%pow n) :call x))
