(ql:quickload '(:alexandria :serapeum) :silent t)
(compile-file "./src/util")
(compile-file "./src/core-simple")
(load "./src/util")
(load "./src/core-simple")

(defpackage cldezero.step27
  (:use :common-lisp)
  (:import-from :cldezero.core-simple
                #:var
                #:fun
                #:forward
                #:backward
                #:call
                #:lookup
                #:var-grad
                #:fun-inputs
                #:add
                #:mul
                #:pow)
  (:import-from :cldezero.util
                #:index
                #:nlist?
                #:map-array)
  (:import-from :serapeum
                #:op))
(in-package :cldezero.step27)


(defclass sine (fun) ())

(defmethod forward ((fun sine) &rest xs)
  (assert (nlist? 1 xs))
  (map-array #'sin (first xs)))

(defmethod backward ((object sine) &rest gys)
  (assert (nlist? 1 gys))
  (let ((x (lookup (first (fun-inputs object))))
        (gy (first gys)))
    (map-array (op (* gy (cos _))) x)))

(defun sine (x)
  (call (make-instance 'sine) x))

(defun fact (n)
  (check-type n index)
  (do ((i n (1- i))
       (acc 1 (* i acc)))
      ((zerop i) acc)))

(defun my-sine (x &optional (threshold 0.0001))
  (let ((y 0))
    (dotimes (i 100000 y)
      (let* ((c (/ (- i) (fact (1+ (* i 2)))))
             (v (pow (mul c x) (+ (* i 2) 1))))
        (setq y (add y v))
        (when (< (aref (lookup v) 0 0) threshold)
          (return y))))))

(let* ((x (var (vector (/ pi 4))))
       (y (my-sine x)))
  (backward y)
  (print (lookup y))
  (print (var-grad x)))
