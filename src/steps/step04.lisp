(ql:quickload '(:alexandria :serapeum) :silent t)

(defpackage cldezero.step04
  (:use :common-lisp)
  (:import-from :alexandria
                #:compose)
  (:import-from :serapeum
                #:op
                #:assure
                #:defmethods))

(in-package cldezero.step04)


;;; Util
(defun map-array (function array &rest more-arrays)
  (let ((arrays (cons array more-arrays))
        (result (make-array (array-dimensions array))))
    (dotimes (i (array-total-size array) result)
      (setf (row-major-aref result i)
            (apply function (mapcar (lambda (arr) (row-major-aref arr i)) arrays))))))


;;; Var
;; Definition
(defclass var ()
  ((data :type array
         :accessor lookup
         :initarg :data)))

;; Constructor
(defun var (x)
  (check-type x array)
  (make-instance 'var :data x))

;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))


;;; Fun
(defclass fun () ())

(defgeneric forward (fun x))

(defun call (fun var)
  (etypecase fun
    (fun
      (var (forward fun (lookup var))))
    (function
      (funcall fun var))))


;; Examples
(defclass square (fun) ())
(defmethod forward ((fun square) x)
  (map-array (op (* _1 _1)) x))

(defclass expn (fun) ())
(defmethod forward ((fun expn) x)
  (map-array #'exp x))

;;; Numerical Diffirentiation
(defun numerical-diff (fun var &optional (eps 1d-4))
  (let* ((x (lookup var))
         (y-e (call fun (var (map-array (op (- _ eps)) x))))
         (y+e (call fun (var (map-array (op (+ _ eps)) x)))))
    (var (map-array (op (/ (- _2 _1) (+ eps eps)))
                    (lookup y-e)
                    (lookup y+e)))))

(print (numerical-diff (make-instance 'square) (var (vector 2.0))))

(print (numerical-diff (op (call (make-instance 'square)
                                 (call (make-instance 'expn)
                                       (call (make-instance 'square) _))))
                       (var (vector 0.5))))
