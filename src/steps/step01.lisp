(ql:quickload '(:alexandria :serapeum) :silent t)

(defpackage cldezero.step01
  (:use :common-lisp))

(in-package :cldezero.step01)


;;; Var
;; Definition
(defclass var ()
  ((data :type array
         :accessor lookup
         :initarg :data)))

;; Constructor
(defun var (x)
  (check-type x array)
  (make-instance 'var :data x))

;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))

;; Examples
(let* ((data (vector 1.0))
       (x (var data)))
  (print (lookup x)))
