(ql:quickload '(:alexandria :serapeum) :silent t)

(defpackage cldezero.step06
  (:use :common-lisp)
  (:import-from :alexandria
                #:compose)
  (:import-from :serapeum
                #:partial
                #:op
                #:assure
                #:defmethods))

(in-package cldezero.step06)


;;; Util
(defun map-array (function array &rest more-arrays)
  (let ((arrays (cons array more-arrays))
        (result (make-array (array-dimensions array))))
    (dotimes (i (array-total-size array) result)
      (setf (row-major-aref result i)
            (apply function (mapcar (lambda (arr) (row-major-aref arr i)) arrays))))))


;;; Var
;; Definition
(defclass var ()
  ((data :type array
         :accessor lookup
         :initarg :data)
   (grad :type array
         :accessor lookup-grad
         :initform nil)))

;; Constructor
(defun var (x)
  (check-type x array)
  (make-instance 'var :data x))

;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))


;;; Fun
(defgeneric forward (fun x))
(defgeneric backward (fun gy))

(defclass fun ()
  ((input :type var
          :accessor fun-input)))

(defun call (fun var)
  (check-type fun fun)
  (check-type var var)
  (setf (fun-input fun) var)
  (var (forward fun (lookup var))))


;; Examples
(defclass square (fun) ())
(defmethods square (fun input)
  (:method forward (fun x)
   (map-array (op (* _1 _1)) x))
  (:method backward (fun gy)
   (map-array (op (* 2 _1 _2)) gy (lookup input))))

(defclass expn (fun) ())
(defmethods expn (fun input)
  (:method forward (fun x)
   (map-array #'exp x))
  (:method backward (fun gy)
   (map-array (op (* (exp _1) _2)) (lookup input) gy)))

(let ((a (make-instance 'square))
      (b (make-instance 'expn))
      (c (make-instance 'square))
      (y nil))
  (setq y
        (funcall (op (call c (call b (call a _))))
                 (var (vector 0.5))))
  (setf (lookup-grad y) (vector 1.0))
  (backward a (backward b (backward c (lookup-grad y)))))
