(ql:quickload '(:alexandria :serapeum) :silent t)

(defpackage cldezero.step07
  (:use :common-lisp)
  (:import-from :alexandria
                #:compose)
  (:import-from :serapeum
                #:partial
                #:op
                #:assure
                #:defmethods))

(in-package cldezero.step07)


;;; Util
(defun map-array (function array &rest more-arrays)
  (let ((arrays (cons array more-arrays))
        (result (make-array (array-dimensions array))))
    (dotimes (i (array-total-size array) result)
      (setf (row-major-aref result i)
            (apply function (mapcar (lambda (arr) (row-major-aref arr i)) arrays))))))


;;; Generic Function
(defgeneric forward (fun x))
(defgeneric backward (self &optional gy))


;;; Var
;; Definition
(defclass var ()
  ((data :type array
         :accessor lookup
         :initarg :data)
   (grad :type array
         :accessor lookup-grad
         :initform nil)
   (creator :type fun
            :accessor lookup-creator
            :initform nil)))

;; Constructor
(defun var (x)
  (check-type x array)
  (make-instance 'var :data x))

(defmethod backward ((var var) &optional _)
  (declare (ignore _))
  (with-slots (creator grad) var
    (when creator
      (with-slots ((prev input)) creator
        (setf (lookup-grad prev) (backward creator grad))
        (backward prev))))  )


;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))


;;; Fun
(defclass fun ()
  ((input :type var
          :accessor fun-input)
   (output :type var
           :accessor fun-output)))

(defun call (fun var)
  (check-type fun fun)
  (check-type var var)
  (let ((output (var (forward fun (lookup var)))))
    (setf (fun-input fun) var
          (lookup-creator output) fun
          (fun-output fun) output)
    output))


;; Examples
(defclass square (fun) ())
(defmethods square (fun input)
  (:method forward (fun x)
   (map-array (op (* _1 _1)) x))
  (:method backward (fun &optional gy)
   (map-array (op (* 2 _1 _2)) gy (lookup input))))

(defclass expn (fun) ())
(defmethods expn (fun input)
  (:method forward (fun x)
   (map-array #'exp x))
  (:method backward (fun &optional gy)
   (map-array (op (* (exp _1) _2)) (lookup input) gy)))

(let* ((funa (make-instance 'square))
       (funb (make-instance 'expn))
       (func (make-instance 'square))
       (x (var (vector 0.5)))
       (a (call funa x))
       (b (call funb a))
       (y (call func b)))

  (assert (eq (lookup-creator y) func))
  (assert (eq (fun-input (lookup-creator y)) b))
  (assert (eq (lookup-creator (fun-input (lookup-creator y))) funb))
  (assert (eq (fun-input (lookup-creator (fun-input (lookup-creator y)))) a))
  (assert (eq (lookup-creator (fun-input (lookup-creator (fun-input (lookup-creator y))))) funa))
  (assert (eq (fun-input (lookup-creator (fun-input (lookup-creator (fun-input (lookup-creator y)))))) x))
  (princ "Pass assert.")

  (setf (lookup-grad y) (vector 1.0))
  (backward y)
  (print (lookup-grad x)))
