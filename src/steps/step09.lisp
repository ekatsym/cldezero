(ql:quickload '(:alexandria :serapeum) :silent t)

(defpackage cldezero.step09
  (:use :common-lisp)
  (:import-from :alexandria
                #:compose)
  (:import-from :serapeum
                #:partial
                #:op
                #:assure
                #:defmethods))

(in-package cldezero.step09)


;;; Util
(defun map-array (function array &rest more-arrays)
  (let ((arrays (cons array more-arrays))
        (result (make-array (array-dimensions array))))
    (dotimes (i (array-total-size array) result)
      (setf (row-major-aref result i)
            (apply function (mapcar (lambda (arr) (row-major-aref arr i)) arrays))))))


;;; Generic Function
(defgeneric forward (fun x))
(defgeneric backward (self &optional gy))


;;; Var
;; Definition
(defclass var ()
  ((data :type array
         :accessor lookup
         :initarg :data)
   (grad :type array
         :accessor lookup-grad
         :initform nil)
   (creator :type fun
            :accessor lookup-creator
            :initform nil)))

;; Constructor
(defun var (x)
  (check-type x array)
  (make-instance 'var :data x))

(defmethod backward ((self var) &optional gy)
  (declare (ignore gy))
  (with-slots (data grad creator) self
    (unless grad
      (setf grad (make-array (array-dimensions data) :initial-element 1.0)))
    (do* ((fun creator                    (lookup-creator in))
          (in  (and fun (fun-input fun))  (and fun (fun-input fun)))
          (out (and fun (fun-output fun)) (and fun (fun-output fun))))
         ((not fun))
         (setf (lookup-grad in)
               (backward fun (lookup-grad out))))))


;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))


;;; Fun
(defclass fun ()
  ((input :type var
          :accessor fun-input)
   (output :type var
           :accessor fun-output)))

(defun call (fun var)
  (check-type fun fun)
  (check-type var var)
  (let ((output (var (forward fun (lookup var)))))
    (setf (fun-input fun) var
          (lookup-creator output) fun
          (fun-output fun) output)
    output))


;; Examples
(defclass square (fun) ())
(defmethods square (fun input)
  (:method forward (fun x)
   (map-array (op (* _1 _1)) x))
  (:method backward (fun &optional gy)
   (map-array (op (* 2 _1 _2)) gy (lookup input))))

(defun square (x)
  (call (make-instance 'square) x))

(defclass expn (fun) ())
(defmethods expn (fun input)
  (:method forward (fun x)
   (map-array #'exp x))
  (:method backward (fun &optional gy)
   (map-array (op (* (exp _1) _2)) (lookup input) gy)))

(defun expn (x)
  (call (make-instance 'expn) x))

(let* ((x (var (vector 0.5)))
       (a (square x))
       (b (expn a))
       (y (square b)))
  (backward y)
  (print (lookup-grad x)))
