(ql:quickload '(:alexandria :serapeum) :silent t)

(defpackage cldezero.step11
  (:use :common-lisp)
  (:import-from :alexandria
                #:compose)
  (:import-from :serapeum
                #:partial
                #:op
                #:assure
                #:defmethods))

(in-package cldezero.step11)


;;; Util
(defun map-array (function array &rest more-arrays)
  (let ((arrays (cons array more-arrays))
        (result (make-array (array-dimensions array))))
    (dotimes (i (array-total-size array) result)
      (setf (row-major-aref result i)
            (apply function (mapcar (lambda (arr) (row-major-aref arr i)) arrays))))))


;;; Generic Function
(defgeneric forward (fun xs))
(defgeneric backward (self &optional gy))


;;; Var
;; Definition
(defclass var ()
  ((data :type array
         :accessor lookup
         :initarg :data)
   (grad :type array
         :accessor lookup-grad
         :initform nil)
   (creator :type fun
            :accessor lookup-creator
            :initform nil)))

;; Constructor
(defun var (x)
  (check-type x array)
  (make-instance 'var :data x))

(defmethod backward ((self var) &optional gy)
  (declare (ignore gy))
  (with-slots (data grad creator) self
    (unless grad
      (setf grad (make-array (array-dimensions data) :initial-element 1.0)))
    (do* ((fun creator                     (lookup-creator in))
          (in  (and fun (fun-inputs fun))  (and fun (fun-inputs fun)))
          (out (and fun (fun-outputs fun)) (and fun (fun-outputs fun))))
         ((not fun))
         (setf (lookup-grad in)
               (backward fun (lookup-grad out))))))


;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))


;;; Fun
(defclass fun ()
  ((inputs :type list
           :accessor fun-inputs)
   (outputs :type list
            :accessor fun-outputs)))

(defun call (fun vars)
  (check-type fun fun)
  (every (op (check-type _1 var)) vars)
  (let ((outputs (mapcar #'var
                         (multiple-value-list
                           (forward fun (mapcar #'lookup vars))))))
    (mapc (lambda (output) (setf (lookup-creator output) fun))
          outputs)
    (setf (fun-inputs fun) vars
          (fun-outputs fun) outputs)
    (apply #'values outputs)))


;; Examples
(defclass add (fun) ())
(defmethod forward ((fun add) xs)
  (reduce (partial #'map-array #'+) xs))

(let ((xs (list (var (vector 2)) (var (vector 3))))
      (f (make-instance 'add)))
  (lookup (call f xs)))
