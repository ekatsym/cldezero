(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '(:alexandria :serapeum) :silent t))

(defpackage cldezero.step19
  (:use :common-lisp)
  (:import-from :alexandria
                #:compose)
  (:import-from :serapeum
                #:partial
                #:op
                #:assure
                #:defmethods))

(in-package cldezero.step19)


;;; Util
(deftype index ()
  `(integer 0 ,array-total-size-limit))

(defun nlist? (n list)
  (check-type list list)
  (check-type n index)
  (do ((i n (1- i))
       (lst list (rest lst)))
      ((or (endp lst) (zerop i))
       (and (endp lst) (zerop i)))))

(defun insert (item list test &key key)
  (check-type list list)
  (check-type test function)
  (check-type key (or function null))
  (let ((test (if (not key)
                  test
                  (op (funcall test (funcall key _1) (funcall key _2))))))
    (do ((rhead '() (cons (first tail) rhead))
         (tail list (rest tail)))
        ((endp tail) (nreverse (cons item rhead)))
        (when (funcall test item (first tail))
          (return (revappend rhead (cons item tail)))))))

(defun map-array (function array &rest more-arrays)
  (let ((arrays (cons array more-arrays))
        (result (make-array (array-dimensions array))))
    (dotimes (i (array-total-size array) result)
      (setf (row-major-aref result i)
            (apply function (mapcar (lambda (arr) (row-major-aref arr i)) arrays))))))

(defun m+ (x y)
  (map-array #'+ x y))

(defun m* (x y)
  (check-type x (array number 2))
  (check-type y (array number 2))
  (assert (= (array-dimension x 1) (array-dimension y 0)))
  (let* ((n (array-dimension x 0))
         (m (array-dimension x 1))
         (l (array-dimension y 1))
         (result (make-array (list n l) :initial-element 0)))
    (dotimes (i n result)
      (dotimes (j m)
        (dotimes (k l)
          (incf (aref result i k)
                (* (aref x i j) (aref y j k))))))))


;;; Special Variables
(defparameter *enable-backprop* t)


;;; Macros
(defmacro with-backprop (enable-p &body body)
  `(let ((*enable-backprop* ,enable-p))
     ,@body))


;;; Generic Function
(defgeneric forward (fun &rest xs))
(defgeneric backward (self &optional save_grad? &rest gys))
(defgeneric clear (self))


;;; Var
;; Definition
(defclass var ()
  ((data       :type     (or array nil)
               :accessor lookup
               :initarg  :data)
   (name       :type     string
               :accessor var-name
               :initarg  :name)
   (grad       :type     (or array nil)
               :accessor var-grad
               :initform nil)
   (creator    :type (or fun nil)
               :accessor var-creator
               :initform nil)
   (generation :type     index
               :accessor generation
               :initform 0)))

;; Constructor
(defun var (x &optional (name ""))
  (check-type x array)
  (check-type name string)
  (make-instance 'var :data x :name name))

;; Methods
(defmethod backward ((self var) &optional save-grad-p &rest gys)
  (declare (ignore gys))
  (with-slots (data grad creator) self
    (unless grad
      (setf grad (make-array (array-dimensions data) :initial-element 1.0)))
    (do ((funs (list creator)))
        ((endp funs))
        (let* ((fun (pop funs))
               (gys (mapcar #'var-grad (fun-outputs fun)))
               (gxs (multiple-value-list (apply #'backward fun nil gys))))
          (mapc (lambda (in gx)
                  (if (null (var-grad in))
                      (setf (var-grad in)
                            gx)
                      (setf (var-grad in)
                            (map-array #'+ (var-grad in) gx)))
                  (flet ((add-fun (f)
                           (when f
                             (setq funs
                                   (remove-duplicates
                                     (insert f funs #'>= :key #'generation))))))
                    (add-fun (var-creator in))))
                (fun-inputs fun)
                gxs)
          (unless save-grad-p
            (dolist (y (fun-outputs fun))
              (setf (var-grad y) nil)))))))

(defmethod clear ((var var))
  (setf (var-grad var) nil))

;; Functions
(defun var-dimensions (var)
  (check-type var var)
  (array-dimensions (lookup var)))

(defun var-total-size (var)
  (check-type var var)
  (array-total-size (lookup var)))

(defun var-element-type (var)
  (check-type var var)
  (array-element-type (lookup var)))

(defun var-len (var)
  (check-type var var)
  (array-dimension (lookup var) 0))

;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))


;;; Fun
;; Definition
(defclass fun ()
  ((inputs     :type list
               :accessor fun-inputs
               :initform '())
   (outputs    :type list
               :accessor fun-outputs
               :initform '())
   (generation :type index
               :accessor generation
               :initform 0)))

;; Functions
(defun call (fun &rest vars)
  (check-type fun fun)
  (every (op (check-type _1 var)) vars)
  (let ((outputs (mapcar #'var
                         (multiple-value-list
                           (apply #'forward fun (mapcar #'lookup vars))))))
    (when *enable-backprop*
      (setf (generation fun) (reduce #'max vars :key #'generation))
      (mapc (lambda (output) (set-creator output fun))
            outputs)
      (setf (fun-inputs fun) (copy-list vars)
            (fun-outputs fun) outputs))
    (apply #'values outputs)))

(defun set-creator (var fun)
  (check-type var var)
  (check-type fun fun)
  (setf (var-creator var) fun
        (generation var)  (1+ (generation fun))))
