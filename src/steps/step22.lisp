(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '(:alexandria :serapeum) :silent t))

(defpackage cldezero.step22
  (:use :common-lisp)
  (:import-from :alexandria
                #:compose)
  (:import-from :serapeum
                #:partial
                #:op
                #:assure
                #:defmethods))

(in-package cldezero.step22)


;;; Util
(deftype index ()
  `(integer 0 ,array-total-size-limit))

(defun nlist? (n list)
  (check-type list list)
  (check-type n index)
  (do ((i n (1- i))
       (lst list (rest lst)))
      ((or (endp lst) (zerop i))
       (and (endp lst) (zerop i)))))

(defun insert (item list test &key key)
  (check-type list list)
  (check-type test function)
  (check-type key (or function null))
  (let ((test (if (not key)
                  test
                  (op (funcall test (funcall key _1) (funcall key _2))))))
    (do ((rhead '() (cons (first tail) rhead))
         (tail list (rest tail)))
        ((endp tail) (nreverse (cons item rhead)))
        (when (funcall test item (first tail))
          (return (revappend rhead (cons item tail)))))))

(defun matrix (rows)
  (every (op (check-type _1 list)) rows)
  (assert (reduce (lambda (len row)
                    (if (and len (= len (length row)))
                        len
                        nil))
                  (rest rows)
                  :initial-value (length (first rows))))
  (make-array (list (length rows) (length (first rows))) :initial-contents rows))

(defun map-array (function array &rest more-arrays)
  (let ((arrays (cons array more-arrays))
        (result (make-array (array-dimensions array))))
    (dotimes (i (array-total-size array) result)
      (setf (row-major-aref result i)
            (apply function (mapcar (lambda (arr) (row-major-aref arr i)) arrays))))))

(defun m+ (x y)
  (map-array #'+ x y))

(defun m* (x y)
  (check-type x (array number 2))
  (check-type y (array number 2))
  (assert (= (array-dimension x 1) (array-dimension y 0)))
  (let* ((n (array-dimension x 0))
         (m (array-dimension x 1))
         (l (array-dimension y 1))
         (result (make-array (list n l) :initial-element 0)))
    (dotimes (i n result)
      (dotimes (j m)
        (dotimes (k l)
          (incf (aref result i k)
                (* (aref x i j) (aref y j k))))))))


;;; Special Variables
(defparameter *enable-backprop* t)


;;; Macros
(defmacro with-backprop (enable-p &body body)
  `(let ((*enable-backprop* ,enable-p))
     ,@body))


;;; Generic Function
(defgeneric forward (fun &rest xs))
(defgeneric backward (object &rest gys))
(defgeneric clear (object))


;;; Var
;; Definition
(defclass var ()
  ((data       :type     (or array nil)
               :accessor lookup
               :initarg  :data)
   (name       :type     string
               :accessor var-name
               :initarg  :name)
   (grad       :type     (or array nil)
               :accessor var-grad
               :initform nil)
   (creator    :type (or fun nil)
               :accessor var-creator
               :initform nil)
   (generation :type     index
               :accessor generation
               :initform 0)))

;; Constructor
(defun var (x &optional (name ""))
  (check-type x array)
  (check-type name string)
  (make-instance 'var :data x :name name))

;; Methods
(defmethod backward ((object var) &rest gys)
  (declare (ignore gys))
  (with-slots (data grad creator) object
    (unless grad
      (setf grad (make-array (array-dimensions data) :initial-element 1.0)))
    (do ((funs (list creator)))
        ((endp funs))
        (let* ((fun (pop funs))
               (gys (mapcar #'var-grad (fun-outputs fun)))
               (gxs (multiple-value-list (apply #'backward fun gys))))
          (mapc (lambda (in gx)
                  (if (null (var-grad in))
                      (setf (var-grad in)
                            gx)
                      (setf (var-grad in)
                            (map-array #'+ (var-grad in) gx)))
                  (flet ((add-fun (f)
                           (when f
                             (setq funs
                                   (remove-duplicates
                                     (insert f funs #'>= :key #'generation))))))
                    (add-fun (var-creator in))))
                (fun-inputs fun)
                gxs)
          (dolist (y (fun-outputs fun))
            (setf (var-grad y) nil))))))

(defmethod clear ((var var))
  (setf (var-grad var) nil))

;; Functions
(defun var-dimensions (var)
  (check-type var var)
  (array-dimensions (lookup var)))

(defun var-total-size (var)
  (check-type var var)
  (array-total-size (lookup var)))

(defun var-element-type (var)
  (check-type var var)
  (array-element-type (lookup var)))

(defun var-len (var)
  (check-type var var)
  (array-dimension (lookup var) 0))

;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))


;;; Fun
;; Definition
(defclass fun ()
  ((inputs     :type list
               :accessor fun-inputs
               :initform '())
   (outputs    :type list
               :accessor fun-outputs
               :initform '())
   (generation :type index
               :accessor generation
               :initform 0)))


;;; Core Functions
(defun call (fun &rest vars)
  (check-type fun fun)
  (every (op (check-type _1 (or var array number))) vars)
  (let* ((inputs (mapcar #'ensure-var vars))
         (outputs (mapcar #'var
                          (multiple-value-list
                            (apply #'forward fun (mapcar #'lookup inputs))))))
    (when *enable-backprop*
      (setf (generation fun) (reduce #'max inputs :key #'generation))
      (mapc (lambda (output) (set-creator output fun))
            outputs)
      (setf (fun-inputs fun) inputs
            (fun-outputs fun) outputs))
    (apply #'values outputs)))

(defun set-creator (var fun)
  (check-type var var)
  (check-type fun fun)
  (setf (var-creator var) fun
        (generation var)  (1+ (generation fun))))


;;; Utility
(defun ensure-var (object)
  (etypecase object
    (var    object)
    (array  (var object))
    (number (var (matrix (list (list object)))))))


;;; Examples
;; Neg
(defclass neg (fun) ())

(defun neg (x)
  (call (make-instance 'neg) x))

(defmethod forward ((fun neg) &rest xs)
  (assert (nlist? 1 xs))
  (map-array #'- (first xs)))

(defmethod backward ((object neg) &rest gys)
  (assert (nlist? 1 gys))
  (map-array #'- (first gys)))

;; Add
(defclass add (fun) ())

(defun add (x y)
  (call (make-instance 'add) x y))

(defmethod forward ((fun add) &rest xs)
  (assert (nlist? 2 xs))
  (map-array #'+ (first xs) (second xs)))

(defmethod backward ((object add) &rest gys)
  (assert (nlist? 1 gys))
  (let ((gy (first gys)))
    (values gy gy)))

;; Sub
(defclass sub (fun) ())

(defun sub (x y)
  (call (make-instance 'sub) x y))

(defmethod forward ((fun sub) &rest xs)
  (assert (nlist? 2 xs))
  (map-array #'- (first xs) (second xs)))

(defmethod backward ((object sub) &rest gys)
  (assert (nlist? 1 gys))
  (let ((gy (first gys)))
    (values gy (map-array #'- gy))))

;; Mul
(defclass mul (fun) ())

(defun mul (x y)
  (call (make-instance 'mul) x y))

(defmethod forward ((fun mul) &rest xs)
  (assert (nlist? 2 xs))
  (map-array #'* (first xs) (second xs)))

(defmethod backward ((object mul) &rest gys)
  (assert (nlist? 1 gys))
  (let ((x0 (first (fun-inputs object)))
        (x1 (second (fun-outputs object)))
        (gy (first gys)))
    (values (map-array #'* x0 gy) (map-array #'* x1 gy))))

;; Div
(defclass div (fun) ())

(defun div (x y)
  (call (make-instance 'div) x y))

(defmethod forward ((fun div) &rest xs)
  (assert (nlist? 2 xs))
  (map-array #'/ (first xs) (second xs)))

(defmethod backward ((object div) &rest gys)
  (assert (nlist? 1 gys))
  (let* ((x0 (first (fun-inputs object)))
         (x1 (second (fun-intpus object)))
         (gy (first gys)))
    (values (map-array #'/ gy x1)
            (map-array (lambda (x0 x1 gy) (* (/ (- x0) (* x1 x1)) gy))
                       x0 x1 gy))))

;; Square
(defclass square (fun) ())

(defun square (x)
  (call (make-instance 'square) x))

(defmethod forward ((fun square) &rest xs)
  (assert (nlist? 1 xs))
  (map-array (op (* _1 _1)) (first xs)))

(defmethod backward ((object square) &rest gys)
  (assert (nlist? 1 gys))
  (let ((gy (first gys)))
    (map-array (lambda (x) (* 2 x gy))
               (first (fun-inputs object)))))

;; Pow
(defclass pow (fun)
  ((c :type number
      :accessor pow-c
      :initarg :c)))

(defun pow (x c)
  (call (make-instance 'pow :c c) x))

(defmethod forward ((fun pow) &rest xs)
  (assert (nlist? 1 xs))
  (let ((x (first xs))
        (c (pow-c fun)))
    (map-array (op (expt _ c)) x)))

(defmethod backward ((object pow) &rest gys)
  (assert (nlist? 1 gys))
  (let ((x (lookup (first (fun-inputs object))))
        (gy (first gys))
        (c (pow-c object)))
    (map-array (lambda (x gy) (* c (expt x (1- c)) gy))
               x
               gy)))


;; Test
(print (neg (var (vector 2.0))))
(print (list (sub 2.0 (var (vector 2.0))) (sub (var (vector 2.0)) 1.0)))
(print (pow (var (vector 2.0)) 3))
