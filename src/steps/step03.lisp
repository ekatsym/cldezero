(ql:quickload '(:alexandria :serapeum) :silent t)

(defpackage cldezero.step03
  (:use :common-lisp)
  (:import-from :serapeum
                #:op
                #:defmethods))

(in-package :cldezero.step03)


;;; Util
(defun map-array (function array &rest more-arrays)
  (let ((arrays (cons array more-arrays))
        (result (make-array (array-dimensions array))))
    (dotimes (i (array-total-size array) result)
      (setf (row-major-aref result i)
            (apply function (mapcar (lambda (arr) (row-major-aref arr i)) arrays))))))


;;; Var
;; Definition
(defclass var ()
  ((data :type array
         :accessor lookup
         :initarg :data)))

;; Constructor
(defun var (x)
  (check-type x array)
  (make-instance 'var :data x))

;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))

;;; Fun
;; Definition
(defclass fun () ())

(defgeneric forward (fun x))

(defun call (fun var)
  (var (forward fun (lookup var))))

;; Examples
(defclass square (fun) ())
(defmethod forward ((fun square) x)
  (map-array (op (* _1 _1)) x))

(defclass expn (fun) ())
(defmethod forward ((fun expn) x)
  (map-array #'exp x))

(let* ((funa (make-instance 'square))
       (funb (make-instance 'expn))
       (func (make-instance 'square))

       (x (var (vector 0.5)))
       (a (call funa x))
       (b (call funb a))
       (y (call func b)))
  (print (lookup y)))
