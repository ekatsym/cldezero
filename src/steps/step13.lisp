(ql:quickload '(:alexandria :serapeum) :silent t)

(defpackage cldezero.step13
  (:use :common-lisp)
  (:import-from :alexandria
                #:compose)
  (:import-from :serapeum
                #:partial
                #:op
                #:assure
                #:defmethods))

(in-package cldezero.step13)


;;; Util
(deftype index ()
  `(integer 0 ,array-total-size-limit))

(defun nlist? (n list)
  (check-type list list)
  (check-type n index)
  (do ((i n (1- i))
       (lst list (rest lst)))
      ((or (endp lst) (zerop i))
       (and (endp lst) (zerop i)))))

(defun map-array (function array &rest more-arrays)
  (let ((arrays (cons array more-arrays))
        (result (make-array (array-dimensions array))))
    (dotimes (i (array-total-size array) result)
      (setf (row-major-aref result i)
            (apply function (mapcar (lambda (arr) (row-major-aref arr i)) arrays))))))

(defun m+ (x y)
  (map-array #'+ x y))

(defun m* (x y)
  (check-type x (array number 2))
  (check-type y (array number 2))
  (assert (= (array-dimension x 1) (array-dimension y 0)))
  (let* ((n (array-dimension x 0))
         (m (array-dimension x 1))
         (l (array-dimension y 1))
         (result (make-array (list n l) :initial-element 0)))
    (dotimes (i n result)
      (dotimes (j m)
        (dotimes (k l)
          (incf (aref result i k)
                (* (aref x i j) (aref y j k))))))))


;;; Generic Function
(defgeneric forward (fun &rest xs))
(defgeneric backward (self &rest gys))


;;; Var
;; Definition
(defclass var ()
  ((data :type array
         :accessor lookup
         :initarg :data)
   (grad :type array
         :accessor lookup-grad
         :initform nil)
   (creator :type fun
            :accessor lookup-creator
            :initform nil)))

;; Constructor
(defun var (x)
  (check-type x array)
  (make-instance 'var :data x))

(defmethod backward ((self var) &rest gys)
  (declare (ignore gys))
  (with-slots (data grad creator) self
    (unless grad
      (setf grad (make-array (array-dimensions data) :initial-element 1.0)))
    (do ((funs (list creator)))
        ((endp funs))
        (let* ((fun (pop funs))
               (gys (mapcar #'lookup-grad (fun-outputs fun)))
               (gxs (multiple-value-list (apply #'backward fun gys))))
          (mapc (lambda (in gx)
                  (setf (lookup-grad in) gx)
                  (let ((new-fun (lookup-creator in)))
                    (when new-fun (push new-fun funs))))
                (fun-inputs fun)
                gxs)))))


;; Printer
(defmethod print-object ((object var) stream)
  (format stream "(VAR ~S)" (lookup object)))


;;; Fun
(defclass fun ()
  ((inputs :type list
           :accessor fun-inputs)
   (outputs :type list
            :accessor fun-outputs)))

(defun call (fun &rest vars)
  (check-type fun fun)
  (every (op (check-type _1 var)) vars)
  (let ((outputs (mapcar #'var
                         (multiple-value-list
                           (apply #'forward fun (mapcar #'lookup vars))))))
    (mapc (lambda (output) (setf (lookup-creator output) fun))
          outputs)
    (setf (fun-inputs fun) (copy-list vars)
          (fun-outputs fun) outputs)
    (apply #'values outputs)))


;; Examples
(defclass add (fun) ())
(defun add (x y)
  (call (make-instance 'add) x y))
(defmethods add (fun)
  (:method forward (fun &rest xs)
   (assert (nlist? 2 xs))
   (apply #'map-array #'+ xs))
  (:method backward (fun &rest gys)
   (assert (nlist? 1 gys))
   (let ((gy (first gys)))
     (values gy gy))))

(defclass square (fun) ())
(defun square (x)
  (call (make-instance 'square) x))
(defmethods square (fun)
  (:method forward (fun &rest xs)
   (assert (nlist? 1 xs))
   (map-array (op (* _1 _1)) (first xs)))
  (:method backward (fun &rest gys)
   (assert (nlist? 1 gys))
   (let ((x (lookup (first (fun-inputs fun))))
         (gy (first gys)))
     (map-array (partial #'* 2) (m* x gy)))))

(let* ((x (var (make-array '(1 1) :initial-element 2.0)))
       (y (var (make-array '(1 1) :initial-element 3.0)))
       (z (add (square x) (square y))))
  (backward z)
  (print (lookup z))
  (print (lookup-grad x))
  (print (lookup-grad y)))
