(defpackage cldezero.core
  (:use :common-lisp)
  (:import-from :cldezero.util
                #:index)
  (:import-from :cldezero.array
                #:map-array)
  (:import-from :alexandria
                #:mappend
                #:compose)
  (:import-from :serapeum
                #:partial)
  (:export #:var
           #:fun))
(in-package :cldezero.core)

;;; Config
(defparameter *enable-backprop* t)

;;; Generic Functions
(defgeneric fun-core (&rest xs))
(defgeneric derivative (&rest xs))

;;; Var
;; Definition
(defclass var ()
  ((data       :type (array number)
               :accessor lookup
               :initarg :data)
   (name       :type string
               :initform ""
               :accessor var-name)
   (gradient   :type (array number)
               :accessor gradient
               :initform '())
   (creator    :type fun
               :accessor creator
               :initform nil)
   (generation :type index
               :accessor generation
               :initform 0)))

;; Utility
(defun backward (var &optional (retain-grad nil))
  (check-type var var)
  (unless (gradient var)
    (setf (gradient var) (make-array (array-dimensions (lookup var)) :initial-element 0)))
  (labels ((%insert (f fs)
             (do ((gen (generation f))
                  (tail fs (rest tail))
                  (rhead '() (cons (first tail) rhead)))
                 ((or (endp tail) (> gen (generation (first tail))))
                  (revappend rhead (cons f tail)))))
           (%collect-funs (fun)
             (do ((funs (list fun) (mappend (compose (partial #'mapcar #'creator)
                                                     #'fun-inputs)
                                            funs))
                  (acc '() (reduce (lambda (a f) (%insert f a))
                                   funs
                                   :initial-value acc))))))
    (let ((funs (%collect-funs (creator var))))
      (mapc (lambda (fun)
              (let* ((inputs (fun-inputs fun))
                     (output (fun-output fun))
                     (gy (gradient output))
                     (dy/dxs (multiple-value-list (apply #'derivative inputs)))
                     (gxs (mapcar (lambda (dy/dx) (map-array #'* gy dy/dx)) dy/dxs)))
                (mapc (lambda (input gx) (setf (gradient input) gx))
                      inputs
                      gxs)))
            funs)))
  var)

(defun clear-grad (var)
  (check-type var var)
  (setf (gradient var) nil))

(defun var-dimensions (var)
  (check-type var var)
  (array-dimensions (lookup var)))

(defun var-rank (var)
  (check-type var var)
  (array-rank (lookup var)))

(defun set-creator (var fun)
  (check-type var var)
  (check-type fun fun)
  (setf (creator var) fun
        (generation var) (1+ (generation fun)))
  fun)

;;; Fun
;; Definition
(defclass fun ()
  (())
  )

;; Utility
