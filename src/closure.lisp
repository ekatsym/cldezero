(defpackage cldezero.closure
  (:use :common-lisp)
  (:import-from :alexandria
                #:lastcar
                #:with-unique-names
                #:once-only)
  (:import-from :serapeum
                #:fbindrec)
  (:import-from :trivia
                #:ematch)
  (:export #:dispatch-lambda
           #:reflective-let
           #:self
           #:csref
           #:with-closure))
(in-package :cldezero.closure)


(defmacro dispatch-lambda (&body clauses)
  (with-unique-names (g!key g!args)
    `(lambda (,g!key &rest ,g!args)
       (ecase ,g!key
         ,@(mapcar (lambda (clause)
                     (destructuring-bind (key (&rest args) . body) clause
                       `(,key (apply (lambda (,@args) ,@body) ,g!args))))
                   clauses)))))

(defmacro reflective-let ((&rest bindings) &body body)
  `(let (,@bindings)
     (fbindrec ((self ,(lastcar body)))
       ,@(butlast body)
       #'self)))

(defun csref (closure parameter)
  (check-type closure function)
  (funcall closure :get parameter))

(defun (setf csref) (new-value closure parameter)
  (check-type closure function)
  (funcall closure :set parameter new-value))

(defun with-closure (parameters closure &rest body)
  (once-only (closure)
    `(symbol-macrolet ,(mapcar (lambda (param)
                                 (ematch param
                                   ((list var par) `(,var (csref ,closure ',par)))
                                   ((list par)     `(,par (csref ,closure ',par)))
                                   (par       `(,par (csref ,closure ',par)))))
                               parameters)
       ,@body)))
