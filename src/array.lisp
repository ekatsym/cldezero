(defpackage cldezero.array
  (:use :common-lisp)
  (:import-from :alexandria
                #:with-unique-names)
  (:import-from :serapeum
                #:partial)
  (:export #:matrix
           #:tensor
           #:doarray
           #:map-into-array
           #:map-array
           #:for-each-array-elements
           #:m+
           #:m*
           #:scal
           #:copy
           #:axpy
           #:dot
           #:nrm2
           #:asum
           #:gemv
           #:ger
           #:gemm))
(in-package :cldezero.array)


;;; Definition Types
(deftype matrix ()
  '(array number 2))

(deftype tensor ()
  '(array number 3))

;;; Utility
(defmacro doarray ((var array &optional (result nil)) &body body)
  (with-unique-names (g!i)
    `(dotimes (,g!i (array-total-size ,array) ,result)
       (let ((,var (row-major-aref ,array ,g!i)))
         ,@body))))

(declaim (inline %map-into-array))
(defun %map-into-array (result-array function &rest arrays)
  (dotimes (i (array-total-size result-array) result-array)
    (setf (row-major-aref result-array i)
          (apply function (mapcar (lambda (a) (row-major-aref a i)) arrays)))))

(defun map-into-array (result-array function &rest arrays)
  (check-type result-array array)
  (check-type function function)
  (every (lambda (a) (check-type a array)) arrays)
  (assert (every (lambda (a)
                   (= (array-total-size result-array)
                      (array-total-size a)))
                 arrays))
  (apply #'%map-into-array result-array function arrays))

(defun map-array (function array &rest more-arrays)
  (check-type array array)
  (check-type function function)
  (every (lambda (a) (check-type a array)) more-arrays)
  (assert (every (lambda (a)
                   (= (array-total-size array)
                      (array-total-size a)))
                 more-arrays))
  (let ((result (make-array (array-dimensions array))))
    (apply #'%map-into-array result function array more-arrays)))

(defun for-each-array-elements (function array &rest more-arrays)
  (check-type function function)
  (check-type array array)
  (every (lambda (a) (check-type a array)) more-arrays)
  (let ((arrays (cons array more-arrays)))
    (dotimes (i (array-total-size array))
      (apply function (mapcar (lambda (a) (row-major-aref a i)) arrays)))))

(defun m+ (x y)
  (check-type x matrix)
  (check-type y matrix)
  (map-array #'+ x y))

(defun m* (x y)
  (check-type x matrix)
  (check-type y matrix)
  (let ((result (make-array (list (array-dimension x 0)
                                  (array-dimension y 1))
                            :initial-element 0)))
    (gemm 1 x y 0 result)))

;;; BLAS
;; Level 1
(defun scal (alpha x)
  (check-type alpha number)
  (check-type x (array number))
  (map-into-array x (partial #'* alpha) x))

(defun copy (x y)
  (check-type x (array number))
  (check-type y (array number))
  (assert (= (array-total-size x) (array-total-size y)) (x y))
  (map-into-array y #'identity x))

(defun axpy (alpha x y)
  (check-type alpha number)
  (check-type x (array number))
  (check-type y (array number))
  (assert (= (array-total-size x) (array-total-size y)) (x y))
  (map-into-array y (lambda (xi yi) (+ (* alpha xi) yi)) x y))

(defun dot (x y)
  (check-type x (array number))
  (check-type y (array number))
  (assert (= (array-total-size x) (array-total-size y)) (x y))
  (let ((result 0))
    (for-each-array-elements (lambda (xi yi) (incf result (* xi yi))) x y)
    result))

(defun nrm2 (x)
  (check-type x (array number))
  (let ((result 0))
    (for-each-array-elements (lambda (xi) (incf result (* xi xi))) x)
    (sqrt result)))

(defun asum (x)
  (check-type x (array number))
  (let ((result 0))
    (for-each-array-elements (lambda (xi) (incf result (abs xi))) x)
    result))

;; Level 2
(defun gemv (alpha a x beta y)
  (check-type alpha number)
  (check-type a (array number))
  (check-type x (array number))
  (check-type y (array number))
  (check-type beta number)
  (assert (= (array-total-size a) (* (array-total-size x) (array-total-size y)))
          (a x y))
  (let ((m (array-total-size y))
        (n (array-total-size x)))
    (dotimes (j m y)
      (setf (row-major-aref y j) (* beta (row-major-aref y j)))
      (dotimes (i n)
        (incf (row-major-aref y j) (* alpha
                                      (row-major-aref a (+ (* j n) i))
                                      (row-major-aref x i)))))))

(defun ger (alpha x y a)
  (check-type alpha number)
  (check-type x (array number))
  (check-type y (array number))
  (check-type a (array number))
  (assert (= (array-total-size a) (* (array-total-size x) (array-total-size y)))
          (a x y))
  (let ((m (array-total-size y))
        (n (array-total-size x)))
    (dotimes (j m y)
      (dotimes (i n)
        (incf (row-major-aref a (+ (* j n) i)) (* alpha
                                                  (row-major-aref x i)
                                                  (row-major-aref y j)))))))

;; Level 3
(defun gemm (alpha a b beta c)
  (check-type alpha number)
  (check-type a (array number))
  (check-type b (array number))
  (check-type beta number)
  (macrolet ((m%gemm (c0 c1)
               `(progn
                  (assert (= (* (array-total-size a) (array-dimension c ,c1))
                             (* (array-total-size b) (array-dimension c ,c0)))
                          (a b c))
                  (assert (zerop (mod (array-total-size a) (array-dimension c ,c0))) (c a))
                  (assert (zerop (mod (array-total-size b) (array-dimension c ,c1))) (b c))
                  (let* ((n (array-dimension c ,c0))
                         (l (array-dimension c ,c1))
                         (m (/ (array-total-size a) n)))
                    (dotimes (i n c)
                      (dotimes (k l)
                        (setf (row-major-aref c (+ (* i l) k))
                              (* beta (row-major-aref c (+ (* i l) k))))
                        (dotimes (j m)
                          (incf (aref c i k) (* alpha
                                                (row-major-aref a (+ (* i m) j))
                                                (row-major-aref b (+ (* j l) k)))))))))))
    (etypecase c
      (matrix (m%gemm 0 1))
      (tensor (cond ((= (array-dimension c 0) 1) (m%gemm 1 2))
                    ((= (array-dimension c 1) 1) (m%gemm 0 2))
                    ((= (array-dimension c 2) 1) (m%gemm 0 1))
                    (t (assert (or (= (array-dimension c 0) 1)
                                   (= (array-dimension c 1) 1)
                                   (= (array-dimension c 2) 1)))))))))
