(defpackage cldezero.core
  (:use :common-lisp)
  (:export #:var
           #:fun))
(in-package :cldezero.core)

;;; Config
(defparameter *enable-backprop* t)

