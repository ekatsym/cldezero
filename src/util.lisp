(defpackage cldezero.util
  (:use :common-lisp)
  (:export #:index
           #:nlist?))

(in-package :cldezero.util)


(deftype index ()
  `(integer 0 ,array-total-size-limit))

(defun nlist? (n list)
  (check-type list list)
  (check-type n index)
  (do ((i n (1- i))
       (lst list (rest lst)))
      ((or (endp lst) (zerop i))
       (and (endp lst) (zerop i)))))
